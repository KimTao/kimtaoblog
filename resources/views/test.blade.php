<!DOCTYPE html>
<html>
<head>
    <title>Welcome</title>
    <link rel="stylesheet" type="text/css" href="./css/app.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <!-- <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2"> -->
      <nav id="navbar_id" class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">KIMTAO_BLOG</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="#">Disabled</a>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <a href="">KIMTAO</a>
          </form>
        </div>
      </nav>
    <!-- </div> -->
    

    <div class="main">
        <div class="container-fluid-lg">
          <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
            <div class="article-list">
              <div class="article-item">
                <div class="row">
                  <div class="col-3">
                    <div class="article-image">
                      <img class="rounded" src="\ckeditor\upload\images\photo_250_155.jpg">
                    </div>
                  </div>
                  <div class="col-9">
                    <div class="article-content">
                      <h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 </h3>
                      <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="line_gray_100"></div>

              <div class="article-item">
                <div class="row">
                  <div class="col-3">
                    <div class="article-image">
                      <img class="rounded" src="\ckeditor\upload\images\photo_250_155.jpg">
                    </div>
                  </div>
                  <div class="col-9">
                    <div class="article-content">
                      <h3>Trải nghiệm nhanh MacBook Air 2020 chip M1</h3>
                      <p>Nội dung chính</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="line_gray_100"></div>

              <div class="article-item">
                <div class="row">
                  <div class="col-3">
                    <div class="article-image">
                      <img class="rounded" src="\ckeditor\upload\images\photo_250_155.jpg">
                    </div>
                  </div>
                  <div class="col-9">
                    <div class="article-content">
                      <h2>Tiêu đề </h2>
                      <p>Nội dung chính</p>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
    </div>

    <script type="text/javascript" src="/js/jquery-min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
</body>
</html>