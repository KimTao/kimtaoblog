@extends ('layouts.master')

@section ('body.content')
<div class="main main_article">
            <div class="main_left">
                <!-- <div class="block_1">
                </div>
                <div class="block_1">
                </div>
                <div class="block_1">
                </div> -->
            </div>
            <div class="main_right">
                 @foreach ($posts as $p)
                <div class="article-item">
                        <div class="article-image">
                          <a href="{{ route('post.show', $p->id) }}"><img src="{{$p->image}}"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href="{{ route('post.show', $p->id) }}"><h3>{{$p->title}}</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                &nbsp;-&nbsp;
                                <span class="article-time"> {{ \Carbon\Carbon::parse($p->created_at)->diffForHumans(\Carbon\Carbon::now()) }} </span>
                                &nbsp;-&nbsp;
                                @foreach ($cats as $c)
                                    @if (strcmp($c->id, $p->category_id)  === 0 )
                                        <a href="" class="post-category"><strong >{{ $c->name_vi }}</strong></a>
                                    @endif
                                @endforeach
                                
                            </div>
                        </div>
                  </div>
                @endforeach
            </div>
        </div>        
@stop

@section ('body.js')
<script type="text/javascript">
            
            $(window).scroll(function() {
               if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
                }
               if($(window).scrollTop() + $(window).height() == $(document).height()) {
                   // alert("Loading new article ...");
                   var request = $.get('/loadmore/' + 20);
                    request.done(function(response){
                        console.log(response);
                        // alert(response);
                    });
               }
            });
        </script>
@stop