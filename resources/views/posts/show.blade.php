@extends ('layouts.master')

@section ('body.content')
<div class="main main_article_detail">

            <div class="article-author-info">
                <img class="author-avatar" src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png">     
                <a href="">{{ $post->author }}</a>
                <i>-</i>
                <a href="" class="article-category">{{ $cat->name_vi }}</a>
                <br>
                <span>{{ \Carbon\Carbon::parse($post->created_at)->diffForHumans(\Carbon\Carbon::now()) }}</span>     
            </div>

            <!-- <div class="article-category-info"> 
                <a href="" class="detail-category">Home</a>
                <span class="detail-category">-</span>
                <a href="" class="detail-category">Xe Hay</a>
            </div> -->
            <div class="article-title-info"> 
                <h1>{{ $post->title }}</h1>
            </div>
            
            <div class="article-detail-info">
                {!! $post->content !!}
            </div>

            <div class="tag-detail">
                <ul class="list-tags-detail">
                    @if ($post->tags != "")
                      @foreach ( explode(',', $post->tags) as $tag ) 
                        <li class="item-tags-detail"><a title="{{ $tag }}" href=""><strong> {{ $tag }} </strong></a></li>
                      @endforeach
                    @endif
<!--                     <li class="item-tags-detail"><a title="" href=""><strong> Công nghệ </strong></a></li>
                    <li class="item-tags-detail"><a title="" href=""><strong> Công nghệ </strong></a></li>
                    <li class="item-tags-detail"><a title="" href=""><strong> Công nghệ </strong></a></li> -->
                </ul>
            </div>

        </div>
@stop
