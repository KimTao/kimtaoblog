@extends ('layouts.master')

@section ('head.title')
Thêm bài viết mới
@stop

@section ('body.content')
<div class="container">
	<div class="row">
		<div class="col-6 offset-3">
			<h1>Add new post</h1>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-6 offset-3">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some ploblems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>

			@endif

			{!! Form::model($post, [
					'route' => ['post.update', $post->id],
					'method' => 'PUT',
					'class' => 'form-horizontal'
				]) 
			!!}
				<div class="form-group">
					{!! Form::label('title', 'Tiêu đề bài viết', ['class' => 'control-label']) !!}
					{!! Form::text('title', null, ['id' => 'title', 'class' => 'form-control', 'placeholder' => 'Tiêu đề bài viết', 'required' => 'true']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('main_content', 'Nội dung chính', ['class' => 'control-label']) !!}
					{!! Form::text('main_content', null, ['id' => 'main_content', 'class' => 'form-control', 'placeholder' => 'Nội dung chính', 'required' => 'true']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('content', 'Nội dung bài viết', ['class' => 'control-label']) !!}
					{!! Form::text('content', null, ['id' => 'content', 'class' => 'form-control', 'placeholder' => 'Nội dung bài viết', 'required' => 'true']) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Sửa bài viết', ['class' => 'btn btn-primary']) !!}
				</div>

			{!! Form::close() !!}

		</div>
	</div>


</div>  
@stop
