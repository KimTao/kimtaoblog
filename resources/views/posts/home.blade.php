<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/fonts/style-fonts.css?v=1">
    <link rel="stylesheet" href="assets/css/app.css?v=1">
    <noscript id="deferred-styles">
        <link rel="stylesheet" href="assets/css/custom.css?v=15">
        <link rel="stylesheet" href="assets/css/slick-theme.css?v=1">
    </noscript>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <link rel="manifest" href="/manifest.json" /><script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>

    <!-- Format page -->
    <script>
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
    </script>
    
    <style>
    img{
        max-width:100%;
    }
    </style>
    </head>
<body >
    <div id="fb-root"></div>
    <!--- Nav start --->
    <div id="techz-navbar-sticky-wrapper" class="sticky-wrapper">
        <nav class="navbar navbar-main navbar-expand navbar-dark bg-primary mb-0" id="techz-navbar">
            <div class="container">
                <a class="navbar-brand" href="/"> <img src="images/logo.png" alt="tin công nghệ, công nghệ, tin tức, techz"></h1> </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse ">
                    <ul class="navbar-nav w-100 justify-content-between">
                                                <li class="nav-item">
                            <a class="nav-link" href="/C/cong-nghe-130"> Công nghệ </a>
                                                    </li>
                                                <li class="nav-item">
                            <a class="nav-link" href="/C/z-xe-131"> Z - Xe </a>
                                                    </li>
                                                <li class="nav-item">
                            <a class="nav-link" href="/C/z-danh-gia-132"> Z - Đánh giá </a>
                                                    </li>
                                                <li class="nav-item">
                            <a class="nav-link" href="/C/z-trend-133"></a>
                                                    </li>
                                                <li class="nav-item">
                            <a class="nav-link" href="/C/z-enews--134"></a>
                                                    </li>
                                                <li class="nav-item">
                            <a class="nav-link" href="/C/kinh-doanh-135"></a>
                                                    </li>
                                                <li class="nav-item search_main">
                            <div class="box_search_main">
                            	<a href="">About</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--- Nav end --->  
<!--- Main start --->
<main role="main">
        <div class="container  mt-4">
        <!-- Index hero start -->
        <section class="row">
            <div class="col-lg-11">
                <div class="fix_slide_one">
                    <div class="slide_one">
                                                <div class="item_slide_one"> 
                            <a href="/143-1020-6-nhin-lai-thanh-cong-vuot-troi-cua-app-y-te-youmed-khi-ap-dung-bizfly-cdn-ylt518245.html"><img class="embed-responsive-item" src="https://media.techz.vn/resize_x260x160/media2019/upload2019/2020/10/26/1_26102020112059.png" onerror="this.src='https://media.techz.vn/resize_x260x160/media2019/upload2019/2019/08/28/nothumb_28082019111817.jpg'" alt="Nhìn lại thành công vượt trội của app y tế YouMed khi áp dụng BizFly CDN"/></a>
                            <a href="/143-1020-6-nhin-lai-thanh-cong-vuot-troi-cua-app-y-te-youmed-khi-ap-dung-bizfly-cdn-ylt518245.html" class="mt-2 my-2 d-block fs16"><h3><strong> Nhìn lại thành công vượt trội của app y tế YouMed khi áp dụng BizFly CDN </strong></h3></a>
                        </div>
                                                <div class="item_slide_one"> 
                            <a href="/178-1020-5-honda-city-ha-gia-re-ngang-hyundai-grand-i10-khach-viet-co-co-hoi-tiet-kiem-ca-tram-trieu-dong-ylt518324.html"><img class="embed-responsive-item" src="https://media.techz.vn/resize_x260x160/media2019/upload2019/2020/10/27/honda-city-giam-gia-re-ngang-hyundai-grand-i10_27102020162136.jpg" onerror="this.src='https://media.techz.vn/resize_x260x160/media2019/upload2019/2019/08/28/nothumb_28082019111817.jpg'" alt="Honda City hạ giá rẻ ngang Hyundai Grand i10, khách Việt có cơ hội tiết kiệm cả trăm triệu đồng"/></a>
                            <a href="/178-1020-5-honda-city-ha-gia-re-ngang-hyundai-grand-i10-khach-viet-co-co-hoi-tiet-kiem-ca-tram-trieu-dong-ylt518324.html" class="mt-2 my-2 d-block fs16"><h3><strong> Honda City hạ giá rẻ ngang Hyundai Grand i10, khách Việt có cơ hội tiết kiệm cả trăm triệu đồng </strong></h3></a>
                        </div>
                                                <div class="item_slide_one"> 
                            <a href="/195-1020-5-mazda-cx-5-mazda3-o-at-giam-soc-toi-ca-tram-trieu-dong-co-hoi-so-huu-xe-cuc-ngon-cho-khach-viet-ylt518321.html"><img class="embed-responsive-item" src="https://media.techz.vn/resize_x260x160/media2019/upload2019/2020/10/27/aa-mazda-anhthumb_27102020152056.jpg" onerror="this.src='https://media.techz.vn/resize_x260x160/media2019/upload2019/2019/08/28/nothumb_28082019111817.jpg'" alt="Mazda CX-5, Mazda3 ồ ạt giảm sốc tới cả trăm triệu đồng, cơ hội sở hữu xe ‘cực ngon’ cho khách Việt"/></a>
                            <a href="/195-1020-5-mazda-cx-5-mazda3-o-at-giam-soc-toi-ca-tram-trieu-dong-co-hoi-so-huu-xe-cuc-ngon-cho-khach-viet-ylt518321.html" class="mt-2 my-2 d-block fs16"><h3><strong> Mazda CX-5, Mazda3 ồ ạt giảm sốc tới cả trăm triệu đồng, cơ hội sở hữu xe ‘cực ngon’ cho khách Việt </strong></h3></a>
                        </div>
                                                <div class="item_slide_one"> 
                            <a href="/192-1020-2-mau-xe-tay-ga-moi-cua-suzuki-gay-sot-khien-honda-vision-run-ray-so-hai-ylt518318.html"><img class="embed-responsive-item" src="https://media.techz.vn/resize_x260x160/media2019/upload2019/2020/10/27/mau-xe-tay-ga-moi-cua-suzuki-gay-sot-khien-honda-vision-run-ray-so-hai-anh-top_27102020151403.jpg" onerror="this.src='https://media.techz.vn/resize_x260x160/media2019/upload2019/2019/08/28/nothumb_28082019111817.jpg'" alt="Mẫu xe tay ga mới của Suzuki gây sốt, khiến Honda Vision run rẩy sợ hãi"/></a>
                            <a href="/192-1020-2-mau-xe-tay-ga-moi-cua-suzuki-gay-sot-khien-honda-vision-run-ray-so-hai-ylt518318.html" class="mt-2 my-2 d-block fs16"><h3><strong> Mẫu xe tay ga mới của Suzuki gây sốt, khiến Honda Vision run rẩy sợ hãi </strong></h3></a>
                        </div>
                                            </div>
                </div>
                

<div class="line_red mb-5"></div>
    <ul class="list-unstyled list-border">
        <li class="media media-lg mb-5">
            <div class="post-square post-wide"> 
            	<a href=""> <img class="embed-responsive-item" src="/images/DauLaDaiLuc/AoTuTap.jpg" alt=""> </a> 
            </div>
            
            <div class="media-body">
                <a href="" class="fs20 d-block mb-2 font-weight-bold"><h4> Mua Honda Winner X với giá chỉ 20 triệu đồng trên trang trực tuyến, chuyện tưởng dại mà lại hóa khôn </h4></a>
                            
             	<div class="post-tags mb-2"> <a href="/C/xe-may-146"><strong class="post-category mr-2"> Xe máy </strong></a> 
             		<small> 8 giờ trước </small> 
             	</div>
                
                <p> (Techz.vn) Honda Winner X đang được rao bán với mức giá rẻ hơn hàng chục triệu đồng trên các trang trực tuyến – “món hời” cho những fan của dòng xe côn tay này. </p>
            </div>
        </li>

        <li class="media media-lg mb-5">
            <div class="post-square post-wide"> 
            	<a href=""> <img class="embed-responsive-item" src="/images/DauLaDaiLuc/AoTuTap.jpg" alt=""> </a> 
            </div>
            
            <div class="media-body">
                <a href="" class="fs20 d-block mb-2 font-weight-bold"><h4> Mua Honda Winner X với giá chỉ 20 triệu đồng trên trang trực tuyến, chuyện tưởng dại mà lại hóa khôn </h4></a>
                            
             	<div class="post-tags mb-2"> <a href="/C/xe-may-146"><strong class="post-category mr-2"> Xe máy </strong></a> 
             		<small> 8 giờ trước </small> 
             	</div>
                
                <p> (Techz.vn) Honda Winner X đang được rao bán với mức giá rẻ hơn hàng chục triệu đồng trên các trang trực tuyến – “món hời” cho những fan của dòng xe côn tay này. </p>
            </div>
        </li>
                                                            
    </ul>
</div>
<div class="col-lg-5 d-none d-lg-block">
</div>
</section>
</div>
</main>
<!--- Main end --->
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="assets/js/slick.min.js" type="text/javascript" defer></script>
        <script src="assets/js/custom.js" type="text/javascript" defer></script>
    </body>
</html>       
