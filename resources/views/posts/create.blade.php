@extends ('layouts.master')

@section ('head.title')
Thêm bài viết mới
@stop

@section ('body.content')
<div class="container">
	<div class="row">
		<div class="col-6 offset-3">
			<h1>Add new post</h1>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-6 offset-3">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some ploblems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>

			@endif

			<!-- <form action="{{ route('post.store') }}" method="POST" class="form-horizontal">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<label for='title' class="control-label">Tiêu đề bài viết</label>
					<input class="form-control" type="text" name="title" id="title" required="true" placeholder="Tiêu đề bài viết">
				</div>
				<div class="form-group">
					<label for='main content' class="control-label">Nội dung chính</label>
					<input class="form-control" type="text" name="main_content" id="main_content" required="true" placeholder="Nội dung chính">
				</div>
				<div class="form-group">
					<label for='content' class="control-label">Nội dung bài viết</label>
					<input class="form-control" type="text" name="content" id="content" required="true" placeholder="Nội dung bài viết">
				</div>
				<div class="form-group">
					<button class="btn btn-primary">Thêm bài viết</button>
				</div>
			</form> -->

			{!! Form::open([
					'route' => ['post.store'],
					'method' => 'POST',
					'class' => 'form-horizontal'
				]) 
			!!}
				<div class="form-group">
					{!! Form::label('title', 'Tiêu đề bài viết', ['class' => 'control-label']) !!}
					{!! Form::text('title', '', ['id' => 'title', 'class' => 'form-control', 'placeholder' => 'Tiêu đề bài viết', 'required' => 'true']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('main_content', 'Nội dung chính', ['class' => 'control-label']) !!}
					{!! Form::text('main_content', '', ['id' => 'main_content', 'class' => 'form-control', 'placeholder' => 'Nội dung chính', 'required' => 'true']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('content', 'Nội dung bài viết', ['class' => 'control-label']) !!}
					{!! Form::text('content', '', ['id' => 'content', 'class' => 'form-control', 'placeholder' => 'Nội dung bài viết', 'required' => 'true']) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Thêm bài viết', ['class' => 'btn btn-primary']) !!}
				</div>

			{!! Form::close() !!}

		</div>
	</div>


</div>  
@stop
