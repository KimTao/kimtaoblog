@extends ('layouts.master')

@section ('head.title')
Homepage
@stop


@section ('body.content')

<div class="fix_slide_one">
    <div class="slide_one">
        @foreach ($slide_posts as $sli_post)
            <div class="item_slide_one"> 
            <a href=""><img class="embed-responsive-item" src="{{ $sli_post->image }}" alt=""/></a>
            <a href="" class="mt-2 my-2 d-block fs16"><h3><strong> {{ $sli_post->title }} </strong></h3></a>
        </div>
        @endforeach
        
    </div>
</div>

<div class="line_red mb-5"></div>
    <ul class="list-unstyled list-border">
        @foreach ($posts as $p)
        <li class="media media-lg mb-5">
            <div class="post-square post-wide"> 
                <a href="{{ route('post.show', $p->id) }}"> <img class="embed-responsive-item" src="{{$p->image}}" alt=""> </a> 
            </div>
            
            <div class="media-body">
                <a href="{{ route('post.show', $p->id) }}" class="fs20 d-block mb-2 font-weight-bold"><h4> {{$p->title}}</h4></a>
                <div class="post-tags mb-2"> 
                    @foreach ($cats as $c)
                        @if (strcmp($c->id, $p->category_id)  === 0 )
                            <a href="/"><strong class="post-category mr-2"> {{ $c->name_vi }} </strong></a> 
                        @endif
                    @endforeach
                    <small> {{ \Carbon\Carbon::parse($p->created_at)->diffForHumans(\Carbon\Carbon::now()) }} </small>
                </div>
                <p> {{$p->main_content}} </p>
            </div>
        </li>
        @endforeach
    </ul>
</div>
@stop
