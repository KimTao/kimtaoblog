@extends ('layouts.master')


@section ('body.content')
<div class="container container-detail">
	<div class="row">
		
        <div class="article-detail-info"> 
            <img class="author-avatar" src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png"> 
            <b><a target="_blank" href="/">{{ $post->author }} </a></b> 
            <i style="opacity: 0.4; margin: 0 5px 0 5px;">-</i> 
            <a class="detail-category" href="">{{ $cat->name_vi }}</a>  
            <br> 
            <span> {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans(\Carbon\Carbon::now()) }}</span> 
        </div>

		<div class="title-detail">
			{{ $post->title }}
		</div>
        <div class="content-detail">
        	<div class="detail-text">
        		{!! $post->content !!}	
        	</div>
        </div>
        <div class="line-dot mb-3 mt-3" ></div>
        <div class="tags-detail" style="width: 100%;" >
                <ul class="list-tags-detail">
                	@if ($post->tags != "")
					  @foreach ( explode(',', $post->tags) as $tag ) 
					    <li class="item-tags-detail"><a title="{{ $tag }}" href=""><strong> {{ $tag }} </strong></a></li>
					  @endforeach
					@endif
                </ul>
        </div>
	</div>
</div>
@stop