<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>@yield('head.title')</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/fonts/style-fonts.css?v=1">
    <link rel="stylesheet" href="/assets/css/app.css?v=1">
    <noscript id="deferred-styles">
        <link rel="stylesheet" href="/assets/css/custom.css?v=15">
        <link rel="stylesheet" href="/assets/css/slick-theme.css?v=1">
    </noscript>
    @yield('head.css')
    </script>
    
    <style>
    img{
        max-width:100%;
    }
    </style>
    </head>
<body >
    <div id="fb-root"></div>
    <!--- Nav start --->
    <div id="techz-navbar-sticky-wrapper" class="sticky-wrapper">
        <nav class="navbar navbar-main navbar-expand navbar-dark bg-primary mb-0" id="techz-navbar">
            <div class="container">
                <a class="navbar-brand" href="/"> <img src="/images/logo.png" alt="tin công nghệ, công nghệ, tin tức, techz"></h1> </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse ">
                    <ul class="navbar-nav w-100">
                        <li class="nav-item">
                            <a class="nav-link" href=""> Nhà đẹp </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href=""> Ô tô </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--- Nav end --->  
<!--- Main start --->
<main role="main">
        <div class="container  mt-4">
        <!-- Index hero start -->
        <section class="row">
            <div class="col-lg-12">
                
                
@yield('body.content')
</section>
</div>
</main>
<!--- Main end --->
        <script src="/assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="/assets/js/slick.min.js" type="text/javascript" defer></script>
        <script src="/assets/js/custom.js" type="text/javascript" defer></script>
        @yield('body.js')
    </body>
</html>       
