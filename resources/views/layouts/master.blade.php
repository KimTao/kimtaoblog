<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home - KimTao</title>
        <link rel="stylesheet" type="text/css" href="/css/custom.css">
        <link rel="stylesheet" type="text/css" href="/css/fontawesome/all.css">
        @yield('head.css')
    </head>

    <body>

        <div class="main">
            <div class="nav">
                <nav class="nav__pc">
                    <ul class="nav__list">
                        <li>
                            <a href="/" class="nav__logo">KIMTAO</a>
                        </li>
                        <li>
                            <a href="" class="nav__link">
                                <i class="fas fa-home"></i> NHÀ ĐẸP
                            </a>
                        </li>
                        <li>
                            <a href="" class="nav__link">
                                <i class="fas fa-car"></i> Ô TÔ
                            </a>
                        </li>
                    </ul>
                </nav>

                <label for="nav__mobile__input" class="nav_button">
                    <i class="fas fa-bars fa-lg"></i>
                </label>
                <input type="checkbox" hidden="true" name="" class="nav__input" id="nav__mobile__input">
                <label  for="nav__mobile__input" class="nav_overlay"></label>

                <nav class="nav__mobile">
                    <ul class="nav__mobile__list">
                        <li>
                            <a href="" class="nav__mobile__link">
                                <i class="fas fa-home"></i> NHÀ ĐẸP
                            </a>
                        </li>
                        <li>
                            <a href="" class="nav__mobile__link">
                                <i class="fas fa-car"></i> Ô TÔ
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        @yield('body.content')
        
        
    </div>

        <script type="text/javascript" src="/js/jquery-min.js"></script>
        @yield('body.js')
    </body>
</html>