<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home - KimTao</title>
        <link rel="stylesheet" type="text/css" href="/css/custom.css">
        <link rel="stylesheet" type="text/css" href="/css/fontawesome/all.css">
    </head>

    <body>

        <div class="main">
            <div class="nav">
                <nav class="nav__pc">
                    <ul class="nav__list">
                        <li>
                            <a href="" class="nav__logo">KIMTAO</a>
                        </li>
                        <li>
                            <a href="" class="nav__link">
                                <i class="fas fa-home"></i> NHÀ ĐẸP
                            </a>
                        </li>
                        <li>
                            <a href="" class="nav__link">
                                <i class="fas fa-car"></i> Ô TÔ
                            </a>
                        </li>
                    </ul>
                </nav>

                <label for="nav__mobile__input" class="nav_button">
                    <i class="fas fa-bars fa-lg"></i>
                </label>
                <input type="checkbox" hidden="true" name="" class="nav__input" id="nav__mobile__input">
                <label  for="nav__mobile__input" class="nav_overlay"></label>

                <nav class="nav__mobile">
                    <ul class="nav__mobile__list">
                        <li>
                            <a href="" class="nav__mobile__link">
                                <i class="fas fa-home"></i> NHÀ ĐẸP
                            </a>
                        </li>
                        <li>
                            <a href="" class="nav__mobile__link">
                                <i class="fas fa-car"></i> Ô TÔ
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        <div class="main main_article_detail">

            <div class="article-author-info">
                <img class="author-avatar" src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png">     
                <a href="">KimTao</a>
                <i>-</i>
                <a href="" class="article-category">Xe Hay</a>
                <br>
                <span>1 ngày trước</span>     
            </div>

            <!-- <div class="article-category-info"> 
                <a href="" class="detail-category">Home</a>
                <span class="detail-category">-</span>
                <a href="" class="detail-category">Xe Hay</a>
            </div> -->
            <div class="article-title-info"> 
                <h1>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h1>
            </div>
            
            <div class="article-detail-info">
                {!! $post->content !!}
            </div>

            <div class="tag-detail">
                <ul class="list-tags-detail">
                    <li class="item-tags-detail"><a title="" href=""><strong> Công nghệ </strong></a></li>
                    <li class="item-tags-detail"><a title="" href=""><strong> Công nghệ </strong></a></li>
                    <li class="item-tags-detail"><a title="" href=""><strong> Công nghệ </strong></a></li>
                </ul>
            </div>

        </div>
    </div>
    </body>
</html>