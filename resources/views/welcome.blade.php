<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home - KimTao</title>
        <link rel="stylesheet" type="text/css" href="/css/custom.css">
        <link rel="stylesheet" type="text/css" href="/css/fontawesome/all.css">
    </head>

    <body>

        <div class="main">
            <div class="nav">
                <nav class="nav__pc">
                    <ul class="nav__list">
                        <li>
                            <a href="" class="nav__logo">KIMTAO</a>
                        </li>
                        <li>
                            <a href="" class="nav__link">
                                <i class="fas fa-home"></i> NHÀ ĐẸP
                            </a>
                        </li>
                        <li>
                            <a href="" class="nav__link">
                                <i class="fas fa-car"></i> Ô TÔ
                            </a>
                        </li>
                    </ul>
                </nav>

                <label for="nav__mobile__input" class="nav_button">
                    <i class="fas fa-bars fa-lg"></i>
                </label>
                <input type="checkbox" hidden="true" name="" class="nav__input" id="nav__mobile__input">
                <label  for="nav__mobile__input" class="nav_overlay"></label>

                <nav class="nav__mobile">
                    <ul class="nav__mobile__list">
                        <li>
                            <a href="" class="nav__mobile__link">
                                <i class="fas fa-home"></i> NHÀ ĐẸP
                            </a>
                        </li>
                        <li>
                            <a href="" class="nav__mobile__link">
                                <i class="fas fa-car"></i> Ô TÔ
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        <div class="main main_article">
            <div class="main_left">
                <div class="block_1">
                </div>
                <div class="block_1">
                </div>
                <div class="block_1">
                </div>
            </div>
            <div class="main_right">


                <div class="article-item">
                        <div class="article-image">
                          <a href=""><img src="\ckeditor\upload\images\photo_250_155.jpg"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href=""><h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                &nbsp;-&nbsp;
                                <span class="article-time"> 1 ngày trước </span>
                                &nbsp;-&nbsp;
                                <a href="" class="post-category"><strong >Ô TÔ</strong></a>
                            </div>
                        </div>
                  </div>

                  <div class="article-item">
                        <div class="article-image">
                          <a href=""><img src="\ckeditor\upload\images\photo_250_155.jpg"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href=""><h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                <a href="" class="post-category"><strong >Ô TÔ</strong></a>
                                <span class="article-time"> 1 ngày trước </span>
                            </div>
                        </div>
                  </div>

                  <div class="article-item">
                        <div class="article-image">
                          <a href=""><img src="\ckeditor\upload\images\photo_250_155.jpg"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href=""><h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                <a href="" class="post-category"><strong >Ô TÔ</strong></a>
                                <span class="article-time"> 1 ngày trước </span>
                            </div>
                        </div>
                  </div>

                  <div class="article-item">
                        <div class="article-image">
                          <a href=""><img src="\ckeditor\upload\images\photo_250_155.jpg"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href=""><h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                <a href="" class="post-category"><strong >Ô TÔ</strong></a>
                                <span class="article-time"> 1 ngày trước </span>
                            </div>
                        </div>
                  </div>

                  <div class="article-item">
                        <div class="article-image">
                          <a href=""><img src="\ckeditor\upload\images\photo_250_155.jpg"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href=""><h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                <a href="" class="post-category"><strong >Ô TÔ</strong></a>
                                <span class="article-time"> 1 ngày trước </span>
                            </div>
                        </div>
                  </div>

                  <div class="article-item">
                        <div class="article-image">
                          <a href=""><img src="\ckeditor\upload\images\photo_250_155.jpg"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href=""><h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                <a href="" class="post-category"><strong >Ô TÔ</strong></a>
                                <span class="article-time"> 1 ngày trước </span>
                            </div>
                        </div>
                  </div>

                  <div class="article-item">
                        <div class="article-image">
                          <a href=""><img src="\ckeditor\upload\images\photo_250_155.jpg"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href=""><h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                <a href="" class="post-category"><strong >Ô TÔ</strong></a>
                                <span class="article-time"> 1 ngày trước </span>
                            </div>
                        </div>
                  </div>

                  <div class="article-item">
                        <div class="article-image">
                          <a href=""><img src="\ckeditor\upload\images\photo_250_155.jpg"></a>
                        </div>
                        <div class="article-content">
                            <div class="article-title">
                                <a href=""><h3>Trải nghiệm nhanh MacBook Air 2020 chip M1 để thấy được sức mạnh công nghệ</h3></a>
                            </div>
                           <!--  <div class="article-main_content">
                                <p>Trải nghiệm nhanh MacBook Air 2020 chip M1</p>
                            </div> -->
                            <div class="article-info">
                                <a href="">
                                    <img src="\ckeditor\upload\images\avatar\123525431_1198856403841027_9023784389733964528_n.png" class="article-avatar_img" alt="">
                                    <span class="article-avatar_author">KimTao</span>    
                                </a>
                                <a href="" class="post-category"><strong >Ô TÔ</strong></a>
                                <span class="article-time"> 1 ngày trước </span>
                            </div>
                        </div>
                  </div>
                  
            </div>
        </div>
    </div>

        <script type="text/javascript" src="/js/jquery-min.js"></script>
        <script type="text/javascript">
            
            $(window).scroll(function() {
               if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
                }
               if($(window).scrollTop() + $(window).height() == $(document).height()) {
                   alert("Loading new article ...");
               }
            });
        </script>
    </body>
</html>