@extends ('layouts.admin_master')

@section ('body.content')
<div class="form-w3layouts">
        <div class="row">
            <div class="col-lg-12">
                    <section class="panel">
            <header class="panel-heading">
                Add Post
            </header>
            <div class="panel-body">
                <form class="form-horizontal bucket-form" action="{{ route('backend.post_addpost') }}" role="form" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-sm-3 control-label ckeditor">Category</label>
                        <div class="col-sm-6">
                            <select class="form-control m-bot15" id="category_id" name="category_id" >
                                @foreach ($category as $c)
                                    @if ( array_search($c, $category->toArray()) === 0)
                                        <option selected="selected" value="{{$c->id}}">{{$c->name_vi}}</option>
                                    @else
                                        <option value="{{$c->id}}">{{$c->name_vi}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Images</label>
                        <div class="col-sm-6">
                            <input type="text" id="image" name="image" class="form-control" placeholder="placeholder">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-6">
                            <input type="text" id="title" name="title" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Main Content</label>
                        <div class="col-sm-6">
                            <input type="text" id="main_content" name="main_content"  class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Content</label>
                        <div class="col-sm-6">
                            <!-- <input type="text" class="form-control round-input"> -->
                            <textarea id="content" name="content" cols="80" rows="20" class="form-control ckeditor"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Author</label>
                        <div class="col-sm-6">
                            <input type="text" id="author" name="author" class="form-control" placeholder="placeholder">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tags</label>
                        <div class="col-sm-6">
                            <input type="text" id="tags" name="tags" class="form-control" placeholder="placeholder">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-6">
                            <div style="text-align: center;">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a href="" class="btn btn-info" > Preview</a>    
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
            </div>
        </div>
        <!-- page end-->
        </div>
@stop
