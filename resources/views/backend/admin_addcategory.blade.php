@extends ('layouts.admin_master')

@section ('body.content')
<div class="form-w3layouts">
        <div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Add Category
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <form role="form" action="{{ route('backend.post_addcategory') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="categoryname">Category Name</label>
                                    <input type="text" class="form-control" id="categoryname" name="categoryname" >
                                </div>
                                <div class="form-group">
                                    <label for="categorynamevi">Category Vietnamese Name</label>
                                    <input type="text" class="form-control" id="categorynamevi" name="categorynamevi">
                                </div>
                                <p style="text-align: center;"><button type="submit" class="btn btn-info">Submit</button></p>
                            </form>
                            </div>

                        </div>
                    </section>

            </div>
        </div>
        <!-- page end-->
        </div>
@stop