<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Preview</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/fonts/style-fonts.css?v=1">
    <link rel="stylesheet" href="/assets/css/app.css?v=1">
    <noscript id="deferred-styles">
        <link rel="stylesheet" href="/assets/css/custom.css?v=15">
        <link rel="stylesheet" href="/assets/css/slick-theme.css?v=1">
    </noscript>
    </script>
    
    <style>
    img{
        max-width:100%;
    }
    </style>
    </head>
<body >
    <div id="fb-root"></div>
    <!--- Nav start --->
    <div id="techz-navbar-sticky-wrapper" class="sticky-wrapper">
        <nav class="navbar navbar-main navbar-expand navbar-dark bg-primary mb-0" id="techz-navbar">
            <div class="container">
                <a class="navbar-brand" href="/"> <img src="/images/logo.png" alt="tin công nghệ, công nghệ, tin tức, techz"></h1> </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse ">
                    <ul class="navbar-nav w-100">
                        <li class="nav-item">
                            <a class="nav-link" href="/C/cong-nghe-130"> Nhà đẹp </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/C/z-xe-131"> Ô tô </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--- Nav end --->  
<!--- Main start --->
<main role="main">
        <div class="container  mt-4">
        <!-- Index hero start -->
        <section class="row">
            <div class="col-lg-12">
                <div class="container container-detail">
                    <div class="row">
                        <div class="article-detail-info"> 
                            <img class="author-avatar" src="/ckeditor/upload/images/avatar/2.png"> 
                            <b><a target="_blank" href="/">{{ $post->author }} </a></b> 
                            <i style="opacity: 0.4; margin: 0 5px 0 5px;">-</i> 
                            <a class="detail-category" href="">{{ $cat->name_vi }}</a>  
                            <br> 
                            <span>Đăng 5 tháng trước</span> 
                        </div>
                        <div class="title-detail">
                            {{ $post->title }}
                        </div>
                        <div class="content-detail">
                            <div class="detail-text">
                                {!! $post->content !!}  
                            </div>
                        </div>
                        <div class="line-dot mb-3 mt-3" ></div>
                        <div class="tags-detail" style="width: 100%;" >
                                <ul class="list-tags-detail">
                                    @if ($post->tags != "")
                                      @foreach ( explode(',', $post->tags) as $tag ) 
                                        <li class="item-tags-detail"><a title="{{ $tag }}" href=""><strong> {{ $tag }} </strong></a></li>
                                      @endforeach
                                    @endif
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
</main>
<!--- Main end --->
        <script src="/assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="/assets/js/slick.min.js" type="text/javascript" defer></script>
        <script src="/assets/js/custom.js" type="text/javascript" defer></script>
    </body>
</html>       
