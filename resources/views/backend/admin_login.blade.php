<!DOCTYPE html>
<head>
<title>Dashboard</title>
<!-- bootstrap-css -->
<link rel="stylesheet" href="{{ asset('/admin_resource/css/bootstrap.min.css')}}" >
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="{{asset('/admin_resource/css/style.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('/admin_resource/css/style-responsive.css')}}" rel="stylesheet"/>
<!-- font CSS -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="{{asset('/admin_resource/css/font.css')}}" type="text/css"/>
<link href="{{asset('/admin_resource/css/font-awesome.css')}}" rel="stylesheet"> 
<!-- //font-awesome icons -->
<script src="{{asset('admin_resource/js/jquery2.0.3.min.js')}}"></script>
</head>
<body>
<div class="log-w3">
<div class="w3layouts-main">
	<h2>Sign In Now</h2>
		@if(Session::get('message'))
		<div class="alert alert-danger">
			<strong>{{ Session::get('message') }}</strong>
			{{ Session::put('message', null) }}
		</div>
		@endif
		<form action=" {{ route('backend.post_login')}}" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<input type="email" class="ggg" name="Email" placeholder="E-MAIL" required="">
			<input type="password" class="ggg" name="Password" placeholder="PASSWORD" required="">
			<span><input type="checkbox" />Remember Me</span>
			<h6><a href="#">Forgot Password?</a></h6>
				<div class="clearfix"></div>
				<input type="submit" value="Sign In" name="login">
		</form>
		<!-- <p>Don't Have an Account ?<a href="registration.html">Create an account</a></p> -->
</div>
</div>
<script src="{{asset('admin_resource/js/bootstrap.js')}}"></script>
<script src="{{asset('admin_resource/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{asset('admin_resource/js/scripts.js')}}"></script>
<script src="{{asset('admin_resource/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('admin_resource/js/jquery.nicescroll.js')}}"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="{{asset('admin_resource/js/jquery.scrollTo.js')}}"></script>
</body>
</html>
