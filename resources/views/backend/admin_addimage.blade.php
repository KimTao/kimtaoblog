@extends ('layouts.admin_master')

@section ('body.content')
<div class="form-w3layouts">
        <div class="row">
            <div class="col-lg-12">
                    <section class="panel">
            <header class="panel-heading">
                Upload Image
            </header>
            <div class="panel-body">
                <form class="form-horizontal bucket-form" action="{{ route('backend.post_addimage') }}" role="form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <textarea id="content" name="content" cols="80" rows="100" class="form-control ckeditor"></textarea>
                        </div>
                    </div>
                </form>
            </div>
        </section>
            </div>
        </div>
        <!-- page end-->
        </div>
@stop
