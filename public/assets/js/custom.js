﻿
jQuery(document).ready(function($) {
    "use strict";
	$(window).scroll(function(){
		if ($(window).scrollTop() >= 32) {
			$('#techz-navbar').addClass('fixed-header');
			$('#techz-navbar-sticky-wrapper').addClass('is-sticky');      
		}
		else {
			$('#techz-navbar').removeClass('fixed-header');
			$('#techz-navbar-sticky-wrapper').removeClass('is-sticky');  
		}
	});	   

    if($('.slide_one').length > 0){
		$('.slide_one').slick({
			  dots: true,
			  infinite: true,
			  speed: 300,
			  slidesToShow: 3,
			  slidesToScroll: 2,
			  variableWidth: true
		});
	}
	if($('.list-slide-zspecial-home').length > 0){
			$('.list-slide-zspecial-home').slick({
				  dots: true,
				  infinite: true,
				  speed: 300,
				  slidesToShow: 3,
				  slidesToScroll: 2,
				  variableWidth: true
			});
	}
});

var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    
var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
