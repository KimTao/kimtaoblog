<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use Session;

class PostManagerController extends Controller
{
    public function get_listpost()
    {
    	$posts = Post::orderBy('id', 'desc')->paginate(20);
    	return view('backend.admin_listpost', compact('posts'));
    }

    public function get_addpost()
    {
    	$category = Category::all();
    	return view('backend.admin_addpost',compact('category'));
    }

    public function post_addpost(Request $req)
    {
    	Post::create([
    		'title' => $req->title,
    		'main_content' => $req->main_content,
    		'content' => $req->content,
    		'category_id' => $req->category_id,
    		'image' => $req->image,
            'author' => $req->author,
            'tags' => $req->tags
    	]);
    	return Redirect()->route('backend.get_listpost');
    }

    public function get_editpost($post_id)
    {
    	$post = Post::find($post_id);
    	$category = Category::all();
    	return view('backend.admin_editpost',compact('post'), compact('category'));
    }

    public function post_editpost(Request $req, $post_id)
    {
    	$post = Post::find($post_id);
    	$post->update([
    		'title' => $req->title,
    		'main_content' => $req->main_content,
    		'content' => $req->content,
    		'category_id' => $req->category_id,
    		'image' => $req->image,
            'author' => $req->author,
            'tags' => $req->tags
    	]);
    	return Redirect()->route('backend.get_listpost');
    }

    public function get_deletepost($post_id)
    {
        $post = Post::find($post_id);
        $post->delete();
    	return Redirect()->route('backend.get_listpost');
    }

    public function get_previewpost($post_id)
    {
        $post = Post::find($post_id);
        $cat = Category::find($post->category_id);
        return view('backend.admin_previewpost', compact('post'))->with('cat', $cat);
    }
}
