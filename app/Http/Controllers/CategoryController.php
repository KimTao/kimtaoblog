<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function get_listcategory()
    {
    	$category = Category::all();
    	return view('backend.admin_listcategory', compact('category'));
    }

    public function get_addcategory()
    {
    	return view('backend.admin_addcategory');
    }
    public function post_addcategory(Request $req)
    {
        $categoryname = $req->categoryname;
        $categorynamevi = $req->categorynamevi;
        Category::create([
            'name' => $categoryname,
            'name_vi' => $categorynamevi
        ]);
        return Redirect()->route('backend.get_listcategory');
    }

    public function get_editcategory($category_id)
    {
        
        $category = Category::find($category_id);
        return view('backend.admin_editcategory', compact('category'));
    }

    public function post_editcategory(Request $req, $category_id)
    {
        echo $category_id;
        echo $req->categoryname;
        echo $req->categorynamevi;
        $category = Category::find($category_id);
        $category->update([
            'name' => $req->categoryname,
            'name_vi' => $req->categorynamevi
        ]);
        return Redirect()->route('backend.get_listcategory');
    }

    public function get_deletecategory($category_id)
    {
        $category = Category::find($category_id);
        $category->delete();
    	return Redirect()->route('backend.get_listcategory');
    }

}
