<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PageController extends Controller
{
    public function index()
    {
    	return redirect('/posts');
    }
    public function welcome()
    {
    	$filesInFolder = \File::files('./images/DauLaDaiLuc/'); 
    	return view('welcome', compact('filesInFolder'));
    }

    public function test()
    {
    	return view('test');
    }

    public function trungtest()
    {
        $post = Post::find(6);
        return view('trungtest')->with('post', $post);
    }
}
