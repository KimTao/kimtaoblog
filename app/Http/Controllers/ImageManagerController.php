<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ImageManagerController extends Controller
{
    public function get_addimage()
    {
    	return view('backend.admin_addimage');
    }

    public function post_addimage(Request $req)
    {

    	$parent_directory = $req->parent_directory;
    	$folder_name = $req->folder_name;

    	if(!is_null($parent_directory))
    	{
    		$path = "upload" . DIRECTORY_SEPARATOR . $parent_directory;
    		if(!File::exists($path))
    		{
    			File::makeDirectory($path);
    		}	
    	}
    	
    	if(!is_null($folder_name))
    	{
    		$path = $path . DIRECTORY_SEPARATOR . $folder_name;
    		if(!File::exists($path))
    		{
    			File::makeDirectory($path);
    		}
    	}
    	
    	if($req->hasFile('image'))
    	{
			$file = $req->file('image');
			$name = $file->getClientOriginalName();
			$file->move($path, $name);
    	}

    	return view('backend.admin_addimage');
    }
}
