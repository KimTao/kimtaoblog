<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AuthUser;
use Illuminate\Support\Facades\Redirect;
use Session;

class AdminController extends Controller
{
    public function get_login(){
    	return view('backend.admin_login');
    }

    public function show_dashboard(){
    	// if(is_null(Session::get('username')))
    	// {
    	// 	return redirect()->route('backend.get_login');
    	// }
    	return view('backend.admin_dashboard');
    }

    public function post_login(Request $req){
    	$email = $req->Email;
    	$password = md5($req->Password);
        
    	$matchThese = ['user_email' => $email, 'password' => $password];
    	$user = AuthUser::where($matchThese)->first();

    	if(is_null($user))
    	{
    		Session::put('message', 'Login Failed!');
    		return redirect()->route('backend.get_login');	
    	}
    	else{
    		Session::put('username', $user->username);
    		Session::put('userid', $user->id);
    		return redirect()->route('backend.show_dashboard');	
    	}
    	
    }

    public function logout(){
    	Session::put('username', null);
    	Session::put('userid', null);
    	return redirect()->route('backend.get_login');
    }
}
