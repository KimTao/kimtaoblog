<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\PostFormRequest;
use Carbon\Carbon;

class PostController extends Controller
{
    public function index()
    {
        Carbon::setLocale('vi');
    	$posts = Post::orderBy('id', 'desc')->paginate(2);
        $cats = Category::all();
        // $slide_posts = Post::orderBy('id', 'desc')->paginate(4);
    	return view('posts.index', compact('posts'))->with('cats', $cats);
    }

    public function loadmore($num)
    {
        echo "KimTao";
        Carbon::setLocale('vi');
        $posts = Post::orderBy('id', 'desc')->paginate(20);
        $cats = Category::all();
        // $slide_posts = Post::orderBy('id', 'desc')->paginate(4);
        return view('posts.index', compact('posts'))->with('cats', $cats);
    }

     public function show($id)
    {
        Carbon::setLocale('vi');
    	$post = Post::find($id);
        $cat = Category::find($post->category_id);
    	return view('posts.show')->with('post', $post)->with('cat', $cat);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit', compact('post'));
    }

    public function update($id, PostFormRequest $request)
    {
        $post = Post::find($id);
        $post->update([
            'title' => Input::get('title'),
            'main_content' => Input::get('main_content'),
            'content' => Input::get('content')
         ]);

        return redirect()->route('post.index');
    }

    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();

        return redirect()->route('post.index');
    }
    public function store(PostFormRequest $request)
    {
        $title = Input::get('title');
        $main_content = Input::get('main_content');
        $content = Input::get('content');

        Post::create([
            'title' => $title,
            'main_content' => $main_content,
            'content' => $content
        ]);

        return redirect()->route('post.index');
    }
}
