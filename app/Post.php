<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
    	'title',
    	'main_content',
    	'content',
    	'category_id',
    	'image',
    	'author',
    	'tags'
    ];
}
