<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');

Route::get('/welcome', [
	'as'	=> 'welcome.index',
	'uses'	=> 'PageController@welcome'
]);

Route::get('/test', [
	'as'	=> 'test.index',
	'uses'	=> 'PageController@test'
]);

Route::get('/trungtest', [
	'as'	=> 'test.trungtest',
	'uses'	=> 'PageController@trungtest'
]);

// post
Route::get('/posts', [
	'as'	=> 'post.index',
	'uses'	=> 'PostController@index'
]);

Route::get('/loadmore/{num}', [
	'as'	=> 'post.loadmore',
	'uses'	=> 'PostController@loadmore'
]);

Route::get('/posts/create', [
	'as'	=> 'post.create',
	'uses'	=> 'PostController@create'
]);

Route::get('/posts/{id}/edit', [
	'as'	=> 'post.edit',
	'uses'	=> 'PostController@edit'
]);

Route::put('/posts/{id}', [
	'as'	=> 'post.update',
	'uses'	=> 'PostController@update'
]);

Route::delete('/posts/{id}', [
	'as'	=> 'post.delete',
	'uses'	=> 'PostController@delete'
]);

Route::post('posts', [
	'as'	=> 'post.store',
	'uses'	=> 'PostController@store'
]);

Route::get('/posts/{id}', [
	'as'	=> 'post.show',
	'uses'	=> 'PostController@show'
]);

//admin
Route::get('/login', [
	'as'	=> 'backend.get_login',
	'uses'	=> 'AdminController@get_login'
]);

Route::post('/login', [
	'as'	=> 'backend.post_login',
	'uses'	=> 'AdminController@post_login'
]);

Route::get('/logout', [
	'as'	=> 'backend.logout',
	'uses'	=> 'AdminController@logout'
]);

Route::get('/dashboard', [
	'as'	=> 'backend.show_dashboard',
	'uses'	=> 'AdminController@show_dashboard'
]);

Route::get('/listcategory', [
	'as'	=> 'backend.get_listcategory',
	'uses'	=> 'CategoryController@get_listcategory'
]);

Route::get('/addcategory', [
	'as'	=> 'backend.get_addcategory',
	'uses'	=> 'CategoryController@get_addcategory'
]);

Route::post('/addcategory', [
	'as'	=> 'backend.post_addcategory',
	'uses'	=> 'CategoryController@post_addcategory'
]);

Route::get('/editcategory/{category_id}', [
	'as'	=> 'backend.get_editcategory',
	'uses'	=> 'CategoryController@get_editcategory'
]);

Route::post('/editcategory/{category_id}', [
	'as'	=> 'backend.post_editcategory',
	'uses'	=> 'CategoryController@post_editcategory'
]);

Route::get('/delete-category/{category_id}', [
	'as'	=> 'backend.get_deletecategory',
	'uses'	=> 'CategoryController@get_deletecategory'
]);

Route::get('/listpost', [
	'as'	=> 'backend.get_listpost',
	'uses'	=> 'PostManagerController@get_listpost'
]);

Route::get('/addpost', [
	'as'	=> 'backend.get_addpost',
	'uses'	=> 'PostManagerController@get_addpost'
]);

Route::post('/addpost', [
	'as'	=> 'backend.post_addpost',
	'uses'	=> 'PostManagerController@post_addpost'
]);

Route::get('/editpost/{post_id}', [
	'as'	=> 'backend.get_editpost',
	'uses'	=> 'PostManagerController@get_editpost'
]);

Route::post('/editpost/{post_id}', [
	'as'	=> 'backend.post_editpost',
	'uses'	=> 'PostManagerController@post_editpost'
]);

Route::get('/delete-post/{post_id}', [
	'as'	=> 'backend.get_deletepost',
	'uses'	=> 'PostManagerController@get_deletepost'
]);

Route::get('/preview/{post_id}', [
	'as'	=> 'backend.get_previewpost',
	'uses'	=> 'PostManagerController@get_previewpost'
]);

Route::get('/addimage', [
	'as'	=> 'backend.get_addimage',
	'uses'	=> 'ImageManagerController@get_addimage'
]);

Route::post('/addimage', [
	'as'	=> 'backend.post_addimage',
	'uses'	=> 'ImageManagerController@post_addimage'
]);