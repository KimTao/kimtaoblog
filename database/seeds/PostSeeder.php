<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 10; ++$i){
        	Post::create([
        		'title' => $faker->sentence,
        		'main_content' => implode('', $faker->sentences(4)),
        		'content' => implode('', $faker->sentences(4))
        	]);
        }
    }
}
